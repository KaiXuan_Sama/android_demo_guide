package com.app.demo_guide;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class GuideActivity extends AppCompatActivity {
    // 计划使用ViewPager来实现引导页的功能
    private ViewPager view_pager;
    private Button bt_start;

    // 一个用来保存布局ID的数组
    private  int[] mLayoutIDs = {
            R.layout.view1,
            R.layout.view2,
            R.layout.viewend,
    };

    List<View> views;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_guide);

        view_pager = findViewById(R.id.view_pager);

        initDate();

        // 设置adapter
        view_pager.setAdapter(pagerAdapter);

        // 获取指定View中的指定控件，并进行绑定
        //      Arrays.binarySearch(mLayoutIDs,R.layout.viewend))这句话的意思就是在"mLayoutIDs"数组中找"R.layout.viewend"这个值并返回
        //      返回的值正好可以给外面的get方法使用，而get的是List中已经解析好的布局文件，所以直接.findViewById进行绑定即可
        bt_start = views.get(Arrays.binarySearch(mLayoutIDs,R.layout.viewend)).findViewById(R.id.bt_start);
        bt_start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // 跳转到新界面
                Intent intent = new Intent(GuideActivity.this,StartActivity.class);
                startActivity(intent);
                finish();
            }
        });

    }

    /**
     * 数据初始化
     */
    private void initDate() {
        // 获取数据
        views = new ArrayList<>();
        for (int i = 0; i < mLayoutIDs.length; i++) {
            // 解析视图并添加进List中
            //      说明：对于一个没有被载入或者想要动态载入的布局，都需要使用LayoutInflater.inflate()来载入；
            views.add(getLayoutInflater().inflate(mLayoutIDs[i],null));
        }

    }

    PagerAdapter pagerAdapter = new PagerAdapter() {
        @Override
        public int getCount() {
            // 返回数据总量（总共有多少页）
            return mLayoutIDs.length;
        }

        @Override
        public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
            return view == object;  // 如果相同，即复用
        }

        @NonNull
        @Override
        public Object instantiateItem(@NonNull ViewGroup container, int position) {
            View view = views.get(position);    // 拿出视图
            container.addView(view);            // 添加视图
            return view;                        // 返回视图
        }

        @Override
        public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
            container.removeView(views.get(position));  // 释放
        }
    };
}
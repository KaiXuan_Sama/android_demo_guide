package com.app.demo_guide;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // 使用SharedPreferences轻量级数据库进行第一次启动的情况保存
        SharedPreferences sp = getPreferences(MODE_PRIVATE);

        // 对是否是第一次进入程序进行判断
        if(sp.getBoolean("ok",true)){
            // 修改数据库中初次启动的数据
            SharedPreferences.Editor editor = sp.edit();
            editor.putBoolean("ok",false);
            editor.apply();

            // 跳转到新界面
            Intent intent = new Intent(MainActivity.this,GuideActivity.class);
            startActivity(intent);
        }else{
            // 跳转到新界面
            Intent intent = new Intent(MainActivity.this,StartActivity.class);
            startActivity(intent);
        }
        finish();
    }
}